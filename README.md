﻿
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_branded-tanks&metric=alert_status)](https://sonarcloud.io/dashboard?id=wot-public-mods_branded-tanks) 
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=wot-public-mods_branded-tanks&metric=ncloc)](https://sonarcloud.io/dashboard?id=wot-public-mods_branded-tanks)
[![Visits](https://gitlab.poliroid.me/api/badge/branded-tanks/visits)](https://gitlab.com/wot-public-mods/branded-tanks)
[![Downloads](https://gitlab.poliroid.me/api/badge/branded-tanks/downloads)](https://gitlab.com/wot-public-mods/branded-tanks/-/releases)
[![Donate](https://cdn.poliroid.me/gitlab/images/donate.svg)](https://poliroid.me/donate)

**Branded Tank Styles** This is a modification for the game "World Of Tanks" that give gui and functional to fast changes of tank Camouflage/Logo/Advert in hangar unto two teams.

### Example of UI for player
![Example of UI for player](https://cdn.poliroid.me/gitlab/images/branding_ui_player.jpg)

### Example of UI for operator
![Example of UI for operator](https://cdn.poliroid.me/gitlab/images/branding_ui_operator.jpg)

### Example of tanks in battle
![Example of tanks in battle](https://cdn.poliroid.me/gitlab/images/branding_ingame.jpg)
