# Changelog

## v4.2.6

* fixes for wot 1.19.1

## v4.2.5

* fixes for wot 1.19.0

## v4.2.4

* improved localization

## v4.2.3

* preview in player mode

## v4.2.2

* cammo_examples
* paint skip logic

## v4.2.1

* fix config save bug

## v4.2.0

* fixed for 1.18.0 wot

## v4.1.4

* fixed for 1.14.1 wot

## v4.1.3

* repo moved to gitlab